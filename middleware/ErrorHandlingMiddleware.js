const ApiError = require('../error/ApiError');

module.exports = (err, req, res, next) => {
  if (err instanceof ApiError) {
    return res.status(err.status).send(err.message);
  }

  return res.status(500).send('Unexpected error!');
}
