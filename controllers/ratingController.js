const ApiError = require('../error/ApiError');
const { Object, User, Transaction, Rating, Favourite } = require('../models/models');
const jwt = require('jsonwebtoken');
const sequelize = require('sequelize');
require('dotenv').config();

const tokenVerify = (token) => jwt.verify(token, process.env.SECRET_KEY);

class RatingController {
  createRating = async (req, res, next) => {
    try {
      const { headers, body } = req;
      const { authorization } = headers;
      const { comment, rating, objectRecipientId, userRecipientId } = body;

      if (body.hasOwnProperty('comment')
      && body.hasOwnProperty('rating')
      && body.hasOwnProperty('objectRecipientId')
      && body.hasOwnProperty('userRecipientId')
      && comment && rating && objectRecipientId && userRecipientId) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const candidateRating = await Rating.findOne({
          where: { [sequelize.Op.and]: [ { objectRecipientId }, { userSenderId: tokenParse.id } ] } 
        });
        if (candidateRating) return next(ApiError.errorUserNotFound('Rating already exists!'));

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth() + 1;
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let now = date + "." + month + "." + year + ' ' + hours + ':' + minutes;

        const objectRecipientIdNum = Number(objectRecipientId);

        const newRating = await Rating.create({
          comment,
          rating,
          date: now,
          objectRecipientId: objectRecipientIdNum,
          userSenderId: tokenParse.id
        });
        newRating.save();

        const userRecipient = await User.findOne({
          where: { id: userRecipientId },
          include: [{
            model: Object,
            include: [{
              model: Rating
            }]
          }]
        });

        let averageRating = 0;
        let sumRating = 0;
        let countRating = 0;
        userRecipient.objects.forEach((element) => {
          if (element.rating) {
            countRating += 1;
            sumRating += element.rating.rating;
          }
        });
        averageRating = Math.round(sumRating/countRating);
        await User.update({
          rating: averageRating
        }, { where: { id: userRecipientId } });
        await Object.update({
          ratingOwner: averageRating
        }, { where: { userId: userRecipientId } });

        return res.send('The review has been saved successfully!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new RatingController();
