const ApiError = require('../error/ApiError');
const { Object, User, Transaction, Rating } = require('../models/models');
const jwt = require('jsonwebtoken');
const sequelize = require('sequelize');
require('dotenv').config();

const tokenVerify = (token) => jwt.verify(token, process.env.SECRET_KEY);

class TransactionController {
  createTransaction = async (req, res, next) => {
    try {  
      const { headers, body } = req;
      const { authorization } = headers;
      const { objectSenderId, objectRecipientId } = body;

      if (body.hasOwnProperty('objectSenderId')
      && body.hasOwnProperty('objectRecipientId')
      && headers.hasOwnProperty('authorization')
      && objectSenderId && objectRecipientId && authorization) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const objectSenderСheck = await Object.findOne({ where: { id: objectSenderId } });
        if (!objectSenderСheck) next(ApiError.errorUserNotFound('The object being sent does not exist!'));

        const objectRecipientСheck = await Object.findOne({ where: { id: objectRecipientId } });
        if (!objectRecipientСheck) next(ApiError.errorUserNotFound('The requested object does not exist!'));

        if (objectSenderСheck.userId !== tokenParse.id) next(ApiError.errorUserNotFound('You are not the owner of the object being sent!'));

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth() + 1;
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let now = date + "." + month + "." + year + ' ' + hours + ':' + minutes;

        const objectRecipient = await Object.findOne({ where: { id: objectRecipientId } });

        const objectSenderIdNum = Number(objectSenderId);
        const objectRecipientIdNum = Number(objectRecipientId);

        const newTransaction = await Transaction.create({
          status: 'waiting',
          date: now,
          objectSenderId: objectSenderIdNum,
          objectRecipientId: objectRecipientIdNum,
          userSenderId: tokenParse.id,
          userRecipientId: objectRecipient.userId
        });
        newTransaction.save();

        return res.send({ data: newTransaction.id });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getMyTransactions = async (req, res, next) => {
    try {
      const { headers } = req;
      const { authorization } = headers;
  
      if (headers.hasOwnProperty('authorization') && authorization) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const result = await Transaction.findAll({
          where: { [sequelize.Op.or]: [{ userSenderId: tokenParse.id }, { userRecipientId: tokenParse.id }] },
          include: [
          {
            model: Object,
            include: [
            {
              model: User
            }]
          },
          {
            model: User,
            include: [
            {
              model: Object
            }]
          }],
          order: [['status', 'DESC'], ['date', 'DESC']]
        });

        return res.send({ data: result });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  deleteTransaction = async (req, res, next) => {
    try {
      const { headers } = req;
      const { id, authorization } = headers;
  
      if (headers.hasOwnProperty('id')
      && headers.hasOwnProperty('authorization')
      && id && authorization) {
        const tokenParse = tokenVerify(authorization);
        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        await Transaction.destroy({ where: { id } });

        return res.send('The transaction was successfully deleted!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getTransactionById = async (req, res, next) => {
    try {
      const { body, headers } = req;
      const { id } = body;
      const { authorization } = headers;

      if (body.hasOwnProperty('id') && id) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) return next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const transactionСheck = await Transaction.findOne({ where: { id } });
        if (!transactionСheck) return next(ApiError.errorUserNotFound('Transaction not found!'));
        
        const userTransСheck = await Transaction.findOne({ where: { [sequelize.Op.and]: [{ id: id }, { 
          [sequelize.Op.or]: [{ userSenderId: tokenParse.id }, { userRecipientId: tokenParse.id }] }] } });
        if (!userTransСheck) return next(ApiError.errorUserNotFound('Your user does not have access to this transaction!'));

        const result = await Transaction.findOne({
          where: { id },
          include: [
          {
            model: Object,
            include: [
              { model: User },
              { model: Rating }
            ]
          },
          {
            model: User,
            include: [{
              model: Object,
              include: [{
                model: Rating
              }]
            }]
          }]
        });
        
        return res.send({ data: [result], userId: tokenParse.id });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  changeStatusTransaction = async (req, res, next) => {
    try {
      const { body, headers } = req;
      const { id, status, objectRecipientId, objectSenderId } = body;
      const { authorization } = headers;

      if (body.hasOwnProperty('status') && status
      && body.hasOwnProperty('id') && id
      && body.hasOwnProperty('objectRecipientId') && objectRecipientId
      && body.hasOwnProperty('objectSenderId') && objectSenderId) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) return next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const transactionСheck = await Transaction.findOne({ where: { id } });
        if (!transactionСheck) return next(ApiError.errorUserNotFound('Transaction not found!'));
        
        const userTransСheck = await Transaction.findOne({ where: { [sequelize.Op.and]: [{ id: id }, { userRecipientId: tokenParse.id }] } });
        if (!userTransСheck) return next(ApiError.errorUserNotFound('You cannot change the transaction status!'));

        let newStatusObject = null;
        if (status === 'true') {
          newStatusObject = false;

          await Object.update({ 
            status: newStatusObject
          }, { where: { id: { [sequelize.Op.in]: [objectSenderId, objectRecipientId] } } });

          await Transaction.update({ 
            status: 'false'
          }, { where: { [sequelize.Op.or]: [{ objectSenderId: { [sequelize.Op.or]: [objectSenderId, objectRecipientId] } }, 
          { objectRecipientId: { [sequelize.Op.or]: [objectSenderId, objectRecipientId] } }] } });
        }

        await Transaction.update({ 
          status
        }, { where: { id } });

        return res.send('The transaction has been successfully updated!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new TransactionController();
