const ApiError = require('../error/ApiError');
const { Object, User, Favourite, Transaction } = require('../models/models');
const jwt = require('jsonwebtoken');
const sequelize = require('sequelize');
require('dotenv').config();
const uuid = require('uuid');
const path = require('path');

const tokenVerify = (token) => jwt.verify(token, process.env.SECRET_KEY);

class ObjectController {
  getAllObject = async (req, res, next) => {
    try {
      const { body } = req;
      const { categoriesFilter, categoriesReceiveFilter, searchByObjectFilter, 
        cityFilter, valueSort, photoFilter, ratingFourFilter } = body;

      if (body.hasOwnProperty('categoriesFilter')
      && body.hasOwnProperty('categoriesReceiveFilter')
      && body.hasOwnProperty('searchByObjectFilter')
      && body.hasOwnProperty('cityFilter')
      && body.hasOwnProperty('valueSort')
      && body.hasOwnProperty('photoFilter')
      && body.hasOwnProperty('ratingFourFilter')) {
        let objFilter = {};
        let masSort = [];

        if (categoriesFilter !== '') {
          objFilter['categories'] = categoriesFilter;
        }
        if (categoriesReceiveFilter !== '') {
          objFilter['categoriesReceive'] = categoriesReceiveFilter;
        }
        if (searchByObjectFilter !== '') {
          objFilter['title'] = { [sequelize.Op.iRegexp]: body.searchByObjectFilter};
        }
        if (cityFilter !== '') {
          objFilter['city'] = { [sequelize.Op.iRegexp]: body.cityFilter};
        }
        if (photoFilter === true) {
          objFilter['imgName'] = { [sequelize.Op.ne]: '' };
        }
        if(ratingFourFilter === true) {
          objFilter['ratingOwner'] = { [sequelize.Op.gte] : 4 };
        }

        if (valueSort === 'asc') {
          masSort.push('date');
          masSort.push('ASC');
        } else if (valueSort === 'desc') {
          masSort.push('date');
          masSort.push('DESC');
        } else if (valueSort === 'rating') {
          masSort.push('ratingOwner');
          masSort.push('ASC');
        }

        let result;
        if (masSort.length !== 0 && objFilter.length !== 0) {
          result = await Object.findAll({ where: { [sequelize.Op.and]: [objFilter] }, order: [masSort], 
            include: [{
              model: User
            }] });
        } else if (masSort.length === 0 && objFilter.length !== 0) {
          result = await Object.findAll({ where: { [sequelize.Op.and]: [objFilter] }, order: [sequelize.fn( 'RANDOM' )], 
          include: [{
            model: User
          }] });
        } else if (objFilter.length === 0 && masSort.length === 0) {
          result = await Object.findAll({ order: [sequelize.fn( 'RANDOM' )], 
          include: [{
            model: User
          }] });
        } else if (objFilter.length === 0 && masSort.length !== 0) {
          result = await Object.findAll({ order: [masSort], 
            include: [{
              model: User
            }] });
        }

        return res.send({ data: result });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  addObject = async (req, res, next) => {
    try {  
      const { headers, body } = req;
      const { authorization } = headers;
      const { title, description, categories, categoriesReceive } = body;

      if (body.hasOwnProperty('title')
      && body.hasOwnProperty('description')
      && body.hasOwnProperty('categories')
      && body.hasOwnProperty('categoriesReceive')
      && headers.hasOwnProperty('authorization')
      && title && description && categories &&
      categoriesReceive && authorization) {
        const tokenParse = tokenVerify(authorization);

        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth() + 1;
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let now = date + "." + month + "." + year + ' ' + hours + ':' + minutes;

        const newObject = await Object.create({
          title,
          description,
          userId: user.id,
          imgName: '',
          status: true,
          categories,
          categoriesReceive,
          date: now,
          city: user.city,
          ratingOwner: user.rating
        });
        newObject.save();

        return res.send({ data: newObject.id });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  addImgForObject = async (req, res, next) => {
    try {
      const { headers, files } = req;
      const { id } = headers;
      const { imgName } = files;
  
      if (headers.hasOwnProperty('id') && id) {
        let end = imgName.name.split('.');
        end = end[end.length - 1];
        let fileName = uuid.v4() + '.' + end;
        imgName.mv(path.resolve(__dirname, '../static', fileName));
        fileName = 'http://localhost:8000/' + fileName;

        await Object.update({ imgName: fileName }, { where: { id } });

        return res.send('The photo was successfully added!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getMyObjects = async (req, res, next) => {
    try {
      const { body, headers } = req;
      const { statusSort } = body;
      const { authorization } = headers;

      if (headers.hasOwnProperty('authorization')
      && body.hasOwnProperty('statusSort')
      && authorization && statusSort) {
        const tokenParse = tokenVerify(authorization);

        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        let result = await Object.findAll({ where: { userId: tokenParse.id },
          order: [['status', statusSort]]});
        
        return res.send({ data: result });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getObjectById = async (req, res, next) => {
    try {
      const { body, headers } = req;
      const { id } = body;
      const { authorization } = headers;

      if (body.hasOwnProperty('id') && id) {
        const tokenParse = tokenVerify(authorization);

        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        let resultObject = await Object.findOne({ where: { id }});

        if (tokenParse.id === resultObject.userId) {
          resultObject.dataValues['ownerStatus'] = true;
        } else {
          resultObject.dataValues['ownerStatus'] = false;
        }

        let resultUser = await User.findOne({ where: { id: resultObject.userId }});

        let resultFavourite = null;
        let findFavourite = await Favourite.findOne({ where: { [sequelize.Op.and]: [{userId: tokenParse.id, objectId: id }] } });
        if (findFavourite) {
          resultFavourite = true;
        } else {
          resultFavourite = false;
        }

        let resultTransaction = null;
        let findTransaction = await Transaction.findOne({ where: { [sequelize.Op.and]: [{userSenderId: tokenParse.id, objectRecipientId: id }] } });
        if (findTransaction) {
          switch(findTransaction.status) {
            case 'true':
              resultTransaction = {status: true, transactionId: findTransaction.id};
              break;
            case 'false':
              resultTransaction = {status: false, transactionId: findTransaction.id};
              break;
            case 'waiting':
              resultTransaction = {status: true, transactionId: findTransaction.id};
              break;
          }
        } else {
          resultTransaction = {status: false, transactionId: -1};
        }

        return res.send({ object: resultObject, user: resultUser, favourite: resultFavourite, transaction: resultTransaction });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  editObject = async (req, res, next) => {
    try {  
      const { body, headers } = req;
      const { id, title, description, categories, categoriesReceive } = body;
      const { authorization } = headers;

      if (body.hasOwnProperty('id')
      && body.hasOwnProperty('title')
      && body.hasOwnProperty('description')
      && body.hasOwnProperty('categories')
      && body.hasOwnProperty('categoriesReceive')
      && headers.hasOwnProperty('authorization')
      && id && title && description && categories && categoriesReceive && authorization) {
        const tokenParse = tokenVerify(authorization);
        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!!'));

        await Object.update({ 
          title,
          description,
          categories,
          categoriesReceive
        }, { where: { id } });

        return res.send('The object has been successfully updated!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  deleteImgObject = async (req, res, next) => {
    try {
      const { body } = req;
      const { id } = body;
  
      if (body.hasOwnProperty('id') && id) {
        await Object.update({ imgName: '' }, { where: { id } });

        return res.send('The photo was successfully deleted!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  deleteObject = async (req, res, next) => {
    try {
      const { headers } = req;
      const { authorization, id } = headers;
  
      if (headers.hasOwnProperty('id')
      && headers.hasOwnProperty('authorization') 
      && id && authorization) {
        const tokenParse = tokenVerify(authorization);
        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        await Object.destroy({ where: { id } });

        return res.send('The object was successfully deleted!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getMyObjectsForModal = async (req, res, next) => {
    try {
      const { headers } = req;
      const { authorization } = headers;

      if (headers.hasOwnProperty('authorization') && authorization) {
        const tokenParse = tokenVerify(authorization);

        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const result = await Object.findAll({ where: { userId: tokenParse.id } });
        
        return res.send({ data: result });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new ObjectController();
