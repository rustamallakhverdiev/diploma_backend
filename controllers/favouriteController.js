const ApiError = require('../error/ApiError');
const { Object, User, Favourite } = require('../models/models');
const jwt = require('jsonwebtoken');
const sequelize = require('sequelize');
require('dotenv').config();

const tokenVerify = (token) => jwt.verify(token, process.env.SECRET_KEY);

class FavouriteController {
  addFavourite = async (req, res, next) => {
    try {  
      const { headers, body } = req;
      const { authorization } = headers;
      const { id } = body;

      if (body.hasOwnProperty('id')
      && headers.hasOwnProperty('authorization')
      && id && authorization) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const favouriteСheck = await Favourite.findOne({ where: { [sequelize.Op.and]: [{userId: tokenParse.id, objectId: id }] } });
        if (favouriteСheck) next(ApiError.errorUserNotFound('Favorites already exist!'));

        const objectСheck = await Object.findOne({ where: { id } });
        if (!objectСheck) next(ApiError.errorUserNotFound('There is no such object!'));

        if (objectСheck.userId === tokenParse.id) next(ApiError.errorUserNotFound('You can not add your own object to favorites!'));

        const newFavourite = await Favourite.create({
          userId: tokenParse.id,
          objectId: id,
        });
        newFavourite.save();

        return res.send('The object has been successfully added to favorites!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  deleteFavourite = async (req, res, next) => {
    try {
      const { headers } = req;
      const { authorization, id } = headers;
  
      if (headers.hasOwnProperty('id')
      && headers.hasOwnProperty('authorization')
      && id && authorization) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const favouriteСheck = await Favourite.findOne({ where: { [sequelize.Op.and]: [{userId: tokenParse.id, objectId: id }] } });
        if (!favouriteСheck) next(ApiError.errorUserNotFound('There is no such favourite!'));

        const objectСheck = await Object.findOne({ where: { id } });
        if (!objectСheck) next(ApiError.errorUserNotFound('There is no such object!'));

        if (objectСheck.userId === tokenParse.id) next(ApiError.errorUserNotFound('You can not delete your own object to favorites!'));

        await Favourite.destroy({ where: { [sequelize.Op.and]: [{userId: tokenParse.id, objectId: id }] } });

        return res.send('The favorite was successfully deleted!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getMyFavourites = async (req, res, next) => {
    try {
      const { headers } = req;
      const { authorization } = headers;
  
      if (headers.hasOwnProperty('authorization')
      && authorization) {
        const tokenParse = tokenVerify(authorization);

        const userСheck = await User.findOne({ where: { id: tokenParse.id } });
        if (!userСheck) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const result = await Favourite.findAll({ 
          where: { userId: tokenParse.id },
          include: [{
            model: Object
          }]
        });

        return res.send({ data: result });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new FavouriteController();
