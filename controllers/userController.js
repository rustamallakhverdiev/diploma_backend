const ApiError = require('../error/ApiError');
const { User, Object, Rating } = require('../models/models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config();
const uuid = require('uuid');
const path = require('path');

const tokenVerify = (token) => jwt.verify(token, process.env.SECRET_KEY);

const generateJwt = (login, id) => {
  return jwt.sign(
    { login, id },
    process.env.SECRET_KEY,
    { expiresIn: '24h' }
  )
}

class UserController {
  registration = async (req, res, next) => {
    try {
      const { body } = req;
      const { fio, login, password, phoneNumber, city } = body;

      if (body.hasOwnProperty('fio')
      && body.hasOwnProperty('login')
      && body.hasOwnProperty('password')
      && body.hasOwnProperty('phoneNumber')
      && body.hasOwnProperty('city')
      && fio && login && password && phoneNumber && city) {
        const candidateLogin = await User.findOne({ where: { login } });
        if (candidateLogin) return next(ApiError.errorUserNotFound('This user already exists!'));

        const candidatePhoneNumber = await User.findOne({ where: {phoneNumber} });
        if (candidatePhoneNumber) return next(ApiError.errorPhoneNumber('A user with this phone number already exists!'));

        if (password.length < 8) return next(ApiError.errorPassword('The password must be longer than 8 characters!'));

        const salt = await bcrypt.genSalt(5);
        const hashPassword = await bcrypt.hash(password, salt);

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth() + 1;
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let now = date + "." + month + "." + year + ' ' + hours + ':' + minutes;

        const newUser = await User.create({
          fio,
          login,
          password: hashPassword,
          imgName: '',
          phoneNumber,
          city,
          date: now,
          rating: 0
        });
        newUser.save();

        const token = generateJwt(newUser.login, newUser.id);

        return res.send({ authorization: token })
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  authorization = async (req, res, next) => {
    try {
      const { body } = req
      const { login, password } = body;

      if (body.hasOwnProperty('login')
      && body.hasOwnProperty('password')
      && login && password) {
        const candidate = await User.findOne({ where: { login } });
        if (!candidate) return next(ApiError.errorUserNotFound('Such user does not exist!'));

        const comparePassword = bcrypt.compareSync(password, candidate.password);
        if (!comparePassword) return next(ApiError.errorPassword('Wrong password!'));

        const token = generateJwt(login, candidate.id);

        return res.send({ authorization: token });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getUserById = async (req, res, next) => {
    try {
      const { body, headers } = req;
      const { id } = body;
      const { authorization } = headers;

      if (body.hasOwnProperty('id')
      && headers.hasOwnProperty('authorization')
      && id && authorization) {
        const tokenParse = tokenVerify(authorization);

        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        let userId = null;
        if (Number(id) === tokenParse.id || id === '-1') {
          userId = "-1";
        } else {
          userId = id;
        }

        let resultUser = null;
        let userStatus = null;
        if (userId !== "-1") {
          resultUser = await User.findOne({
            where: { id: userId },
            include: [{
              model: Object,
              include: [{
                model: Rating,
                include: [{
                  model: User
                }]
              }]
            }]
          });
          userStatus = false;
        } else if (userId === "-1") {
          resultUser = await User.findOne({
            where: { id: tokenParse.id },
            include: [{
              model: Object,
              include: [{
                model: Rating,
                include: [{
                  model: User
                }]
              }]
            }]
          });
          userStatus = true;
        }

        return res.send({ data: [resultUser], userStatus });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  getUserByIdForEdit = async (req, res, next) => {
    try {
      const { headers } = req;
      const { authorization } = headers;

      if (headers.hasOwnProperty('authorization') && authorization) {
        const tokenParse = tokenVerify(authorization);

        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        const result = await User.findOne({
          where: { id: tokenParse.id }
        });

        return res.send({ data: result });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  editUser = async (req, res, next) => {
    try {  
      const { body, headers } = req;
      const { id, fio, city, phoneNumber } = body;
      const { authorization } = headers;

      if (body.hasOwnProperty('id')
      && body.hasOwnProperty('fio')
      && body.hasOwnProperty('city')
      && body.hasOwnProperty('phoneNumber')
      && id && fio && city && phoneNumber) {
        const tokenParse = tokenVerify(authorization);
        const user = await User.findOne({ where: { id: tokenParse.id } });
        if (!user) next(ApiError.errorUserNotFound('You are not authorized or there is no such user!'));

        await User.update({ 
          fio,
          city,
          phoneNumber,
        }, { where: { id } });

        return res.send('The user has been successfully updated!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  deleteImgUser = async (req, res, next) => {
    try {
      const { body } = req;
      const { id } = body;
  
      if (body.hasOwnProperty('id') && id) {
        await User.update({ imgName: '' }, { where: { id } });

        return res.send('The photo was successfully deleted!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  addImgForUser = async (req, res, next) => {
    try {
      const { headers, files } = req;
      const { id } = headers;
      const { imgName } = files;
  
      if (headers.hasOwnProperty('id') && id) {
        let end = imgName.name.split('.');
        end = end[end.length - 1];
        let fileName = uuid.v4() + '.' + end;
        imgName.mv(path.resolve(__dirname, '../static', fileName));
        fileName = 'http://localhost:8000/' + fileName;

        await User.update({ imgName: fileName }, { where: { id } });

        return res.send('The photo was successfully added!');
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  changePasswordByOldAndNew = async (req, res, next) => {
    try {
      const { headers, body } = req;
      const { authorization } = headers;
      const { oldPassword, newPassword } = body;
  
      if (headers.hasOwnProperty('authorization')
      && body.hasOwnProperty('oldPassword')
      && body.hasOwnProperty('newPassword')
      && authorization && oldPassword && newPassword) {
        const tokenParse = tokenVerify(authorization);

        const candidate = await User.findOne( { where: { id: tokenParse.id } });
        if (!candidate) return next(ApiError.errorUserNotFound('The user is not logged in!'));

        const compareOldPassword = bcrypt.compareSync(oldPassword, candidate.password);
        if (!compareOldPassword) return next(ApiError.errorPassword('Wrong old password!'));

        if (newPassword.length < 8) return next(ApiError.errorPassword('The new password must be longer than 8 characters!'));

        const salt = await bcrypt.genSalt(5);
        const hashNewPassword = await bcrypt.hash(newPassword, salt);

        await User.update({ password: hashNewPassword }, { where: { id: tokenParse.id } });

        return res.send({ data: candidate.id });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  changePasswordByNew = async (req, res, next) => {
    try {
      const { headers, body } = req;
      const { authorization } = headers;
      const { password } = body;
  
      if (headers.hasOwnProperty('authorization')
      && body.hasOwnProperty('password')
      && authorization && password) {
        const tokenParse = tokenVerify(authorization);

        const candidate = await User.findOne( { where: { id: tokenParse.id } });
        if (!candidate) return next(ApiError.errorUserNotFound('The user is not logged in!'));

        if (password.length < 8) return next(ApiError.errorPassword('The password must be longer than 8 characters!'));

        const salt = await bcrypt.genSalt(5);
        const hashPassword = await bcrypt.hash(password, salt);

        await User.update({ password: hashPassword }, { where: { id: tokenParse.id } });

        return res.send({ data: candidate.id });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  checkLoginToChangePassword = async (req, res, next) => {
    try {
      const { headers, body } = req;
      const { authorization } = headers;
      const { login } = body;
  
      if (headers.hasOwnProperty('authorization')
      && body.hasOwnProperty('login')
      && authorization && login) {
        const tokenParse = tokenVerify(authorization);

        const candidate = await User.findOne( { where: { id: tokenParse.id } });
        if (!candidate) return next(ApiError.errorUserNotFound('The user is not logged in!'));

        if (candidate.login !== login) return next(ApiError.errorPassword('Incorrect login!'));

        return res.send({ data: true });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  checkLoginAndPhoneNumberToChangePassword = async (req, res, next) => {
    try {
      const { body } = req;
      const { login, phoneNumber } = body;
  
      if (body.hasOwnProperty('login')
      && body.hasOwnProperty('phoneNumber')
      && login && phoneNumber) {
        const candidate = await User.findOne( { where: { login, phoneNumber } });
        if (!candidate) return next(ApiError.errorUserNotFound('Incorrect data!'));

        return res.send({ data: candidate.id });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  changePasswordByLoginAndPhoneNumber = async (req, res, next) => {
    try {
      const { body } = req;
      const { password, id } = body;
  
      if (body.hasOwnProperty('password')
      && body.hasOwnProperty('id')
      && password && id) {
        const candidate = await User.findOne( { where: { id } });
        if (!candidate) return next(ApiError.transmittedValuesNotFound());

        if (password.length < 8) return next(ApiError.errorPassword('The password must be longer than 8 characters!'));

        const salt = await bcrypt.genSalt(5);
        const hashPassword = await bcrypt.hash(password, salt);

        await User.update({ password: hashPassword }, { where: { id: candidate.id } });

        return res.send({ data: true });
      } else {
        return next(ApiError.transmittedValuesNotFound());
      }
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new UserController();
