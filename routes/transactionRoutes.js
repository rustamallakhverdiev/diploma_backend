const Router = require('express');
const router = new Router();
const TransactionController = require('../controllers/transactionController');

router.post('/createTransaction', TransactionController.createTransaction);
router.get('/getMyTransactions', TransactionController.getMyTransactions);
router.delete('/deleteTransaction', TransactionController.deleteTransaction);
router.post('/getTransactionById', TransactionController.getTransactionById);
router.patch('/changeStatusTransaction', TransactionController.changeStatusTransaction);

module.exports = router;
