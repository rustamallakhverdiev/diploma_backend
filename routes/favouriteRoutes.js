const Router = require('express');
const router = new Router();
const FavouriteController = require('../controllers/favouriteController');

router.post('/addFavourite', FavouriteController.addFavourite);
router.delete('/deleteFavourite', FavouriteController.deleteFavourite);
router.get('/getMyFavourites', FavouriteController.getMyFavourites);

module.exports = router;
