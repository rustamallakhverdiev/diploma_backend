const Router = require('express');
const router = new Router();

const userRoutes = require('./userRoutes');
const objectRoutes = require('./objectRoutes');
const favouriteRoutes = require('./favouriteRoutes');
const ratingRoutes = require('./ratingRoutes');
const transactionRoutes = require('./transactionRoutes');

router.use('/user', userRoutes);
router.use('/object', objectRoutes);
router.use('/favourite', favouriteRoutes);
router.use('/rating', ratingRoutes);
router.use('/transaction', transactionRoutes);

module.exports = router;
