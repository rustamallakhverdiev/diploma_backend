const Router = require('express');
const router = new Router();
const RatingController = require('../controllers/ratingController');

router.post('/createRating', RatingController.createRating);

module.exports = router;
