const Router = require('express');
const router = new Router();
const ObjectController = require('../controllers/objectController');

router.post('/getAllObject', ObjectController.getAllObject);
router.post('/addObject', ObjectController.addObject);
router.patch('/addImgForObject', ObjectController.addImgForObject);
router.post('/getMyObjects', ObjectController.getMyObjects);
router.post('/getObjectById', ObjectController.getObjectById);
router.patch('/editObject', ObjectController.editObject);
router.patch('/deleteImgObject', ObjectController.deleteImgObject);
router.delete('/deleteObject', ObjectController.deleteObject);
router.get('/getMyObjectsForModal', ObjectController.getMyObjectsForModal);

module.exports = router;
