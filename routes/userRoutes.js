const Router = require('express');
const router = new Router();
const UserController = require('../controllers/userController');

router.post('/authorization', UserController.authorization);
router.post('/registration', UserController.registration);
router.post('/getUserById', UserController.getUserById);
router.get('/getUserByIdForEdit', UserController.getUserByIdForEdit);
router.patch('/editUser', UserController.editUser);
router.patch('/deleteImgUser', UserController.deleteImgUser);
router.patch('/addImgForUser', UserController.addImgForUser);
router.patch('/changePasswordByOldAndNew', UserController.changePasswordByOldAndNew);
router.patch('/changePasswordByNew', UserController.changePasswordByNew);
router.post('/checkLoginToChangePassword', UserController.checkLoginToChangePassword);
router.post('/checkLoginAndPhoneNumberToChangePassword', UserController.checkLoginAndPhoneNumberToChangePassword);
router.patch('/changePasswordByLoginAndPhoneNumber', UserController.changePasswordByLoginAndPhoneNumber);


module.exports = router;
