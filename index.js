require('dotenv').config();
const express = require('express');
const cors = require('cors');
const sequelize = require('./db');
const models = require('./models/models');
const router = require('./routes/index');
const fileupload = require('express-fileupload');
const ErrorHandler = require('./middleware/ErrorHandlingMiddleware');
const path = require('path');

const app = express();
app.use(cors());
app.use(express.static(path.resolve(__dirname, 'static')));
app.use(express.json());
app.use(fileupload({}));
app.use('/', router);


app.use(ErrorHandler);

const PORT = process.env.PORT || 8000;

const start = async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync();
    app.listen(PORT, () => console.log(`Example app on port ${PORT}!`));
  } catch (e) {
    console.log(e);
  }
}

start();
