const sequelize = require('../db');
const { DataTypes } = require('sequelize');

const User = sequelize.define('user', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
  fio: { type: DataTypes.STRING, allowNull: false },
  login: { type: DataTypes.STRING, unique: true, allowNull: false },
  password: { type: DataTypes.STRING, allowNull: false },
  imgName: { type: DataTypes.STRING },
  phoneNumber: { type: DataTypes.STRING, unique: true, allowNull: false },
  city: { type: DataTypes.STRING, allowNull: false },
  date: { type: DataTypes.STRING, allowNull: false },
  rating: { type: DataTypes.INTEGER },
})

const Object = sequelize.define('object', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
  title: { type: DataTypes.STRING, allowNull: false },
  description: { type: DataTypes.TEXT, allowNull: false },
  imgName: { type: DataTypes.STRING },
  status: { type: DataTypes.BOOLEAN, allowNull: false },
  categories: { type: DataTypes.STRING, allowNull: false },
  categoriesReceive: { type: DataTypes.STRING, allowNull: false },
  date: { type: DataTypes.STRING, allowNull: false },
  city: { type: DataTypes.STRING, allowNull: false },
  ratingOwner: { type: DataTypes.INTEGER }
  // userId
})

const Favourite = sequelize.define('favourite', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true }
  // userId,
  // objectId
})

const Transaction = sequelize.define('transaction', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  status: { type: DataTypes.STRING },
  date: { type: DataTypes.STRING, allowNull: false },
  objectSenderId: { type: DataTypes.INTEGER },
  userRecipientId: { type: DataTypes.INTEGER }
  // userSenderId
  // objectRecipientId
})

const Rating = sequelize.define('rating', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  comment: { type: DataTypes.TEXT },
  rating: { type: DataTypes.INTEGER },
  date: { type: DataTypes.STRING, allowNull: false }
  // userSenderId
  // objectRecipientId
})

User.hasMany(Object);
Object.belongsTo(User);

User.hasMany(Favourite);
Favourite.belongsTo(User);
Object.hasMany(Favourite);
Favourite.belongsTo(Object);

User.hasMany(Transaction, { foreignKey: 'userSenderId' });
Transaction.belongsTo(User, { foreignKey: 'userSenderId' });
Object.hasMany(Transaction, { foreignKey: 'objectRecipientId' });
Transaction.belongsTo(Object, { foreignKey: 'objectRecipientId' });

User.hasMany(Rating, { foreignKey: 'userSenderId' });
Rating.belongsTo(User, { foreignKey: 'userSenderId' });
Object.hasOne(Rating, { foreignKey: 'objectRecipientId' });
Rating.belongsTo(Object, { foreignKey: 'objectRecipientId' });

module.exports = {
  User,
  Object,
  Favourite,
  Rating,
  Transaction
}
