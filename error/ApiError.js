class ApiError extends Error {
  constructor(status, message) {
    super();
    this.status = status;
    this.message = message;
  }

  static badRequest (message) {
    return new ApiError(400, message);
  }

  static notFound () {
    return new ApiError(404, 'Not found!');
  }

  static serverError () {
    return new ApiError(500, 'Server error!');
  }

  static transmittedValuesNotFound () {
    return new ApiError(422, 'Invalid data entered!');
  }

  static notImplemented () {
    return new ApiError(501, 'Not implemented!');
  }

  static errorUserNotFound (message) {
    return new ApiError(421, message);
  }

  static errorPassword (message) {
    return new ApiError(423, message);
  }

  static errorPhoneNumber (message) {
    return new ApiError(424, message);
  }
}

module.exports = ApiError;
